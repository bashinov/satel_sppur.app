namespace BarsUp.Frontend.Host
{
    using BarsUp;
    using BarsUp.Utils;

	/// <summary>
    /// Манифест ресурсов модуля
    /// </summary>
    public class ResourceManifest : ResourceManifestBase
    {
        protected override void BaseInit(IResourceManifestContainer container)
        {
            AddResource(container, "libs\\B4\\autostart\\FrontendHost.js");
            AddResource(container, "Content\\favicon.ico");
        }
        
        private void AddResource(IResourceManifestContainer container, string path)
        {
            var webPath = path.Replace("\\", "/");
            var resourceName = webPath.Replace("/", ".");

            container.Add(webPath, "BarsUp.Frontend.Host.dll/BarsUp.Frontend.Host.{0}".FormatUsing(resourceName));
        }
    }
}
