Ext.define('B4.autostart.WebHost', {
    singleton: true,
    requires: [
        'BarsUp.flexdesk.viewport.Default', ],
    onBeforeCreate: {
        autostartAfter: ['B4.autostart.FlexDeskAutostart'],
        fn: function(cfg) {
            cfg.mainView = 'BarsUp.flexdesk.viewport.Default';
            cfg.mainViewConfig = {
                "splitter": {}
            };
        }
    }
});