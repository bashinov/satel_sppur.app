namespace Satel.Sppur.Mappings
{
    using NHibernate;
    using NHibernate.Type;
    using NHibernate.Mapping.ByCode;    
    using NHibernate.Mapping.ByCode.Conformist;    
    using BarsUp.DataAccess;
    
    using BarsUp.DataAccess.ByCode;
    using BarsUp.DataAccess.UserTypes; 
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Modules.PostgreSql.DataAccess.UserTypes;
    using BarsUp.Modules.Versioning.Map;

    /// <summary>
    /// Мапинг сущности Источники данных
    /// </summary>
    public class DataSourceMap : ClassMapping<Satel.Sppur.DataSource>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public DataSourceMap()         {
			Polymorphism(PolymorphismType.Explicit);
            Table("DATASOURCE");
            
            Id(x => x.Id, map => { 
    map.Column("id");
    map.Type(new Int64Type());
    map.Generator(new BarsUp.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
 });
            
#region Class: DataSource
Property(x => x.NameFull, p => { p.Column(col => { col.Name("NAMEFULL");});});
Property(x => x.NameCut, p => { p.Column(col => { col.Name("NAMECUT");});});
Property(x => x.Comment, p => { p.Column(col => { col.Name("COMMENT");});});
#endregion
        }
    }

    }

