namespace Satel.Sppur.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using BarsUp.Utils;
    using BarsUp.Ecm7.Framework;          
    using BarsUp.Modules.NH.Migrations.DatabaseExtensions;
    using BarsUp.Designer.GeneratedApp.Migrations;
    using BarsUp.Modules.PostgreSql;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using BarsUp.Modules.PostgreSql.Migrations;      
    using BarsUp.Modules.Versioning.Extensions;      	                   

    /// <summary>
    /// Миграция 2021.04.04.00-50-54
    /// </summary>
    [BarsUp.Ecm7.Framework.Migration("2021.04.04.00-50-54")]
    	
    public class Ver20210404005054 : InterceptedBaseMigration
    {
        /// <summary>
        /// Накатить миграцию
        /// </summary>
        public override void MigrateUp()
        {
            Database.AddTable(new SchemaQualifiedObjectName { Schema ="public", Name="INDICATORPROPERTIES"});
Database.ExecuteNonQuery("COMMENT ON TABLE INDICATORPROPERTIES is E\'\"Меры показателя\"\'");
Database.AddTable(new SchemaQualifiedObjectName { Schema ="public", Name="DATASOURCE"});
Database.ExecuteNonQuery("COMMENT ON TABLE DATASOURCE is E\'\"Источники данных\"\'");
Database.AddTable(new SchemaQualifiedObjectName { Schema ="public", Name="UNITMEASURE"});
Database.ExecuteNonQuery("COMMENT ON TABLE UNITMEASURE is E\'\"Единицы измерения\"\'");
Database.AddColumn(new SchemaQualifiedObjectName { Schema ="public", Name="INDICATORPROPERTIES"}, new Column("id", DbType.Int64.AsColumnType(), ColumnProperty.Identity | ColumnProperty.PrimaryKey));
Database.ExecuteNonQuery("COMMENT ON COLUMN INDICATORPROPERTIES.id is E\'\"Идентификатор\"\'");
Database.AddColumn(new SchemaQualifiedObjectName { Schema ="public", Name="INDICATORPROPERTIES"}, new Column("object_create_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
Database.ExecuteNonQuery("COMMENT ON COLUMN INDICATORPROPERTIES.object_create_date is E\'\"Дата создания\"\'");
Database.AddColumn(new SchemaQualifiedObjectName { Schema ="public", Name="INDICATORPROPERTIES"}, new Column("object_edit_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
Database.ExecuteNonQuery("COMMENT ON COLUMN INDICATORPROPERTIES.object_edit_date is E\'\"Дата изменения\"\'");
Database.AddColumn(new SchemaQualifiedObjectName { Schema ="public", Name="INDICATORPROPERTIES"}, new Column("object_version", DbType.Int32.AsColumnType(), ColumnProperty.None, 0));
Database.ExecuteNonQuery("COMMENT ON COLUMN INDICATORPROPERTIES.object_version is E\'\"Версия\"\'");
Database.AddColumn(new SchemaQualifiedObjectName { Schema ="public", Name="DATASOURCE"}, new Column("id", DbType.Int64.AsColumnType(), ColumnProperty.Identity | ColumnProperty.PrimaryKey));
Database.ExecuteNonQuery("COMMENT ON COLUMN DATASOURCE.id is E\'\"Идентификатор\"\'");
Database.AddColumn(new SchemaQualifiedObjectName { Schema ="public", Name="DATASOURCE"}, new Column("namefull", DbType.String.AsColumnType().WithLength(2147483647), ColumnProperty.None, "\'\'"));
Database.ExecuteNonQuery("COMMENT ON COLUMN DATASOURCE.namefull is E\'\"Наименование полное\"\'");
Database.AddColumn(new SchemaQualifiedObjectName { Schema ="public", Name="DATASOURCE"}, new Column("namecut", DbType.String.AsColumnType().WithLength(2147483647), ColumnProperty.None, "\'\'"));
Database.ExecuteNonQuery("COMMENT ON COLUMN DATASOURCE.namecut is E\'\"Наименование краткое\"\'");
Database.AddColumn(new SchemaQualifiedObjectName { Schema ="public", Name="DATASOURCE"}, new Column("comment", DbType.String.AsColumnType().WithLength(2147483647), ColumnProperty.None, "\'\'"));
Database.ExecuteNonQuery("COMMENT ON COLUMN DATASOURCE.comment is E\'\"Описание\"\'");
Database.AddColumn(new SchemaQualifiedObjectName { Schema ="public", Name="UNITMEASURE"}, new Column("id", DbType.Int64.AsColumnType(), ColumnProperty.Identity | ColumnProperty.PrimaryKey));
Database.ExecuteNonQuery("COMMENT ON COLUMN UNITMEASURE.id is E\'\"Идентификатор\"\'");
Database.AddColumn(new SchemaQualifiedObjectName { Schema ="public", Name="UNITMEASURE"}, new Column("name", DbType.String.AsColumnType().WithLength(2147483647), ColumnProperty.None, "\'\'"));
Database.ExecuteNonQuery("COMMENT ON COLUMN UNITMEASURE.name is E\'\"Наимнование\"\'");
Database.AddColumn(new SchemaQualifiedObjectName { Schema ="public", Name="UNITMEASURE"}, new Column("sign", DbType.String.AsColumnType().WithLength(2147483647), ColumnProperty.None, "\'\'"));
Database.ExecuteNonQuery("COMMENT ON COLUMN UNITMEASURE.sign is E\'\"Обозначение\"\'");
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "INDICATORPROPERTIES" }, "object_create_date", false);
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "INDICATORPROPERTIES" }, "object_edit_date", false);
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "INDICATORPROPERTIES" }, "object_version", false);
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "DATASOURCE" }, "namefull", false);
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "DATASOURCE" }, "namecut", false);
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "DATASOURCE" }, "comment", false);
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "UNITMEASURE" }, "name", false);
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "UNITMEASURE" }, "sign", false);
        }

        /// <summary>
        /// Откатить миграцию
        /// </summary>
        public override void MigrateDown()
        {
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "UNITMEASURE" }, "sign", true);
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "UNITMEASURE" }, "name", true);
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "DATASOURCE" }, "comment", true);
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "DATASOURCE" }, "namecut", true);
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "DATASOURCE" }, "namefull", true);
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "INDICATORPROPERTIES" }, "object_version", true);
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "INDICATORPROPERTIES" }, "object_edit_date", true);
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "INDICATORPROPERTIES" }, "object_create_date", true);
Database.RemoveColumn(new SchemaQualifiedObjectName { Schema ="public", Name="UNITMEASURE"}, "sign");
Database.RemoveColumn(new SchemaQualifiedObjectName { Schema ="public", Name="UNITMEASURE"}, "name");
Database.RemoveColumn(new SchemaQualifiedObjectName { Schema ="public", Name="DATASOURCE"}, "comment");
Database.RemoveColumn(new SchemaQualifiedObjectName { Schema ="public", Name="DATASOURCE"}, "namecut");
Database.RemoveColumn(new SchemaQualifiedObjectName { Schema ="public", Name="DATASOURCE"}, "namefull");
Database.RemoveColumn(new SchemaQualifiedObjectName { Schema ="public", Name="INDICATORPROPERTIES"}, "object_version");
Database.RemoveColumn(new SchemaQualifiedObjectName { Schema ="public", Name="INDICATORPROPERTIES"}, "object_edit_date");
Database.RemoveColumn(new SchemaQualifiedObjectName { Schema ="public", Name="INDICATORPROPERTIES"}, "object_create_date");
Database.RemoveTable(new SchemaQualifiedObjectName { Schema ="public", Name="UNITMEASURE"});
Database.RemoveTable(new SchemaQualifiedObjectName { Schema ="public", Name="DATASOURCE"});
Database.RemoveTable(new SchemaQualifiedObjectName { Schema ="public", Name="INDICATORPROPERTIES"});
        
        }        

            }

    }