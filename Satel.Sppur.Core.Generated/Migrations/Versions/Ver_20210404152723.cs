namespace Satel.Sppur.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using BarsUp.Utils;
    using BarsUp.Ecm7.Framework;          
    using BarsUp.Modules.NH.Migrations.DatabaseExtensions;
    using BarsUp.Designer.GeneratedApp.Migrations;
    using BarsUp.Modules.PostgreSql;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using BarsUp.Modules.PostgreSql.Migrations;      
    using BarsUp.Modules.Versioning.Extensions;      	                   

    /// <summary>
    /// Миграция 2021.04.04.15-27-23
    /// </summary>
    [BarsUp.Ecm7.Framework.Migration("2021.04.04.15-27-23")]
    [MigrationDependsOn(typeof(Ver20210405230249))]
	
    public class Ver20210404152723 : InterceptedBaseMigration
    {
        /// <summary>
        /// Накатить миграцию
        /// </summary>
        public override void MigrateUp()
        {
            Database.AddTable(new SchemaQualifiedObjectName { Schema ="public", Name="DATASOURCE"});
Database.ExecuteNonQuery("COMMENT ON TABLE DATASOURCE is E\'\"Источники данных\"\'");
Database.AddColumn(new SchemaQualifiedObjectName { Schema ="public", Name="DATASOURCE"}, new Column("id", DbType.Int64.AsColumnType(), ColumnProperty.Identity | ColumnProperty.PrimaryKey));
Database.ExecuteNonQuery("COMMENT ON COLUMN DATASOURCE.id is E\'\"Идентификатор\"\'");
Database.AddColumn(new SchemaQualifiedObjectName { Schema ="public", Name="DATASOURCE"}, new Column("namefull", DbType.String.AsColumnType().WithLength(2147483647), ColumnProperty.None, "\'\'"));
Database.ExecuteNonQuery("COMMENT ON COLUMN DATASOURCE.namefull is E\'\"Наименование полное\"\'");
Database.AddColumn(new SchemaQualifiedObjectName { Schema ="public", Name="DATASOURCE"}, new Column("namecut", DbType.String.AsColumnType().WithLength(2147483647), ColumnProperty.None, "\'\'"));
Database.ExecuteNonQuery("COMMENT ON COLUMN DATASOURCE.namecut is E\'\"Наименование краткое\"\'");
Database.AddColumn(new SchemaQualifiedObjectName { Schema ="public", Name="DATASOURCE"}, new Column("comment", DbType.String.AsColumnType().WithLength(2147483647), ColumnProperty.None, "\'\'"));
Database.ExecuteNonQuery("COMMENT ON COLUMN DATASOURCE.comment is E\'\"Описание\"\'");
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "DATASOURCE" }, "namefull", false);
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "DATASOURCE" }, "namecut", false);
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "DATASOURCE" }, "comment", false);
        }

        /// <summary>
        /// Откатить миграцию
        /// </summary>
        public override void MigrateDown()
        {
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "DATASOURCE" }, "comment", true);
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "DATASOURCE" }, "namecut", true);
Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName { Schema = "public", Name = "DATASOURCE" }, "namefull", true);
Database.RemoveColumn(new SchemaQualifiedObjectName { Schema ="public", Name="DATASOURCE"}, "comment");
Database.RemoveColumn(new SchemaQualifiedObjectName { Schema ="public", Name="DATASOURCE"}, "namecut");
Database.RemoveColumn(new SchemaQualifiedObjectName { Schema ="public", Name="DATASOURCE"}, "namefull");
Database.RemoveTable(new SchemaQualifiedObjectName { Schema ="public", Name="DATASOURCE"});
        
        }        

            }

    }