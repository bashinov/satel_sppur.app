
namespace Satel.Sppur
{
    using System;
    using BarsUp.DataAccess;
    using System.Linq;
    using NHibernate;

    
    /// <summary>
    /// Источники данных
    /// 
    /// </summary>
    [BarsUp.Utils.Display(@"Источники данных")]
    [BarsUp.Utils.Description(@"")]  
    [System.Runtime.InteropServices.GuidAttribute("a5cdbc25-bb2c-42d7-a1c6-e81c9171e117")]    
    [BarsUp.Utils.Attributes.Uid("a5cdbc25-bb2c-42d7-a1c6-e81c9171e117")]
        public class DataSource : BarsUp.DataAccess.PersistentObject
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public DataSource() : base() {
                    }
        
        /// <summary>
/// Наименование полное
/// </summary>
[BarsUp.Utils.Display("Наименование полное")]
[BarsUp.Utils.Attributes.Uid("e052d138-d463-4df9-8053-3cd41fd05c7d")]
public virtual System.String NameFull{ get; set; }
/// <summary>
/// Наименование краткое
/// </summary>
[BarsUp.Utils.Display("Наименование краткое")]
[BarsUp.Utils.Attributes.Uid("0cd3d854-08e9-427b-bc9c-194139dbca16")]
public virtual System.String NameCut{ get; set; }
/// <summary>
/// Описание
/// </summary>
[BarsUp.Utils.Display("Описание")]
[BarsUp.Utils.Attributes.Uid("573db6de-9ac5-4b27-8fd4-bad7a199c036")]
public virtual System.String Comment{ get; set; }
        
            }

    }

