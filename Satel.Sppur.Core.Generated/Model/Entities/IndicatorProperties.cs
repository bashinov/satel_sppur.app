
namespace Satel.Sppur
{
    using System;
    using BarsUp.DataAccess;
    using System.Linq;
    using NHibernate;

    
    /// <summary>
    /// Меры показателя
    /// 
    /// </summary>
    [BarsUp.Utils.Display(@"Меры показателя")]
    [BarsUp.Utils.Description(@"")]  
    [System.Runtime.InteropServices.GuidAttribute("5d162aa2-dbe8-4d69-b7cc-199b7d5a7f40")]    
    [BarsUp.Utils.Attributes.Uid("5d162aa2-dbe8-4d69-b7cc-199b7d5a7f40")]
        public class IndicatorProperties : BarsUp.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public IndicatorProperties() : base() {
                    }
        
                
            }

    }

