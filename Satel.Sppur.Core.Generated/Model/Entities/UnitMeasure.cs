
namespace Satel.Sppur
{
    using System;
    using BarsUp.DataAccess;
    using System.Linq;
    using NHibernate;

    
    /// <summary>
    /// Единицы измерения
    /// 
    /// </summary>
    [BarsUp.Utils.Display(@"Единицы измерения")]
    [BarsUp.Utils.Description(@"")]  
    [System.Runtime.InteropServices.GuidAttribute("ef61ab46-f7f1-46b6-9187-45b4f62ba98f")]    
    [BarsUp.Utils.Attributes.Uid("ef61ab46-f7f1-46b6-9187-45b4f62ba98f")]
        public class UnitMeasure : BarsUp.DataAccess.PersistentObject
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public UnitMeasure() : base() {
                    }
        
        /// <summary>
/// Наимнование
/// </summary>
[BarsUp.Utils.Display("Наимнование")]
[BarsUp.Utils.Attributes.Uid("cdefe874-1e7b-48b3-b771-0789f32a8548")]
public virtual System.String Name{ get; set; }
/// <summary>
/// Обозначение
/// </summary>
[BarsUp.Utils.Display("Обозначение")]
[BarsUp.Utils.Attributes.Uid("751f1042-c4a8-4135-8411-51e3fabecd98")]
public virtual System.String Sign{ get; set; }
        
            }

    }

