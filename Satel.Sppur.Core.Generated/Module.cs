namespace Satel.Sppur.Core.Generated
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Castle.Windsor;
    using Castle.MicroKernel.Registration;
    using Microsoft.Extensions.DependencyInjection;
    using BarsUp.DataAccess;
	using BarsUp.Extensions.Configuration;
	using BarsUp.Extensions.Reflection;
    using BarsUp.Core.Application;
    
    using Satel.Sppur;
    using BarsUp.Application;
    using BarsUp.Windsor;
    using BarsUp;
    using BarsUp.Events;
    using BarsUp.IoC;
    using BarsUp.Registrar;
    
    using BarsUp.Designer.Core;

    using BarsUp.Core;

    using BarsUp.Modules.Security;

    using BarsUp.DataAccess;

    using BarsUp.Modules.Mapping.NHibernate;

    using BarsUp.Modules.PostgreSql;

    using BarsUp.Modules.ChangeRequest;

    using BarsUp.Modules.Versioning;

    using BarsUp.Modules.Workflow.Generated;

    using BarsUp.Modules.EntityVersioning;

    using BarsUp.Modules.Fias;

    using BarsUp.Modules.RuntimeFilters;

    using BarsUp.Designer.GeneratedApp;
    using BarsUp.Utils;

    /// <summary>
    /// Класс подключения модуля
    /// </summary>
    [BarsUp.Utils.Display("СППУР [Core] (Генерируемый проект)")]    
    [BarsUp.Utils.Attributes.Uid("47bd5752-b532-7f65-0b98-5968a7d2a9e1")]
    public class Module : AssemblyDefinedModule, IAspNetCoreServiceConfigurator
    {        
        /// <summary>
        /// Загрузка модуля
        /// </summary>
        public override void Install()
        {
            RegisterDataComponents();
RegisterInterceptors();
AssemblyReplacedTypeResolver.AddAssemblyReplace("Satel.Sppur", GetType().Assembly.GetName().Name);
            
        }        
        
        protected override void SetDependencies()
        {
            base.SetDependencies();
            DependsOn<BarsUp.Core.Module>();
DependsOn<BarsUp.Designer.GeneratedApp.Module>();
DependsOn<BarsUp.Modules.ChangeRequest.Module>();
DependsOn<BarsUp.Modules.EntityVersioning.Module>();
DependsOn<BarsUp.Modules.RuntimeFilters.Module>();
DependsOn<BarsUp.Modules.Security.Module>();
DependsOn<BarsUp.Modules.Workflow.Generated.Module>();
            
        }

        public void ConfigureServices(IServiceCollection services)
        {
            
            
        }

        private void RegisterDataComponents(){
    Container.RegisterSingleton<BarsUp.IModuleDependencies, Satel.Sppur.ModuleDependencies>();
    Container.RegisterTransient<BarsUp.DataAccess.INhibernateConfigModifier, Satel.Sppur.NHibernateConfigurator>();
    NHibernateConfigurator.RegisterAll();

}
private void RegisterInterceptors(){
    Container.RegisterDomainInterceptor<Satel.Sppur.DataSource, DataSourceDomainServiceInterceptor>();
    Container.RegisterDomainInterceptor<Satel.Sppur.IndicatorProperties, IndicatorPropertiesDomainServiceInterceptor>();
    Container.RegisterDomainInterceptor<Satel.Sppur.UnitMeasure, UnitMeasureDomainServiceInterceptor>();

}
    }
}
