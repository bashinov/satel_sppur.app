namespace Satel.Sppur
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using BarsUp;
        using BarsUp.Modules.Scripting.Interfaces;
    using BarsUp.Modules.Scripting.Services;
    using BarsUp.Modules.Security;
    using BarsUp.DataAccess;

    internal class UnitMeasureDomainServiceInterceptor : AsyncDomainInterceptor<UnitMeasure>
    {
	    private readonly IScriptExecutor _scriptExecutor;        

		public UnitMeasureDomainServiceInterceptor(IScriptExecutor scriptExecutor)
        {
            _scriptExecutor = scriptExecutor;            
        }

												
		private void RunScript(UnitMeasure entity, string scriptResourceName, string eventDisplayName)
		{
			var scriptContext = new BaseScriptContext(Container);
            scriptContext.SetValue("entity", entity);
            
			try
            {
                _scriptExecutor.RunFromResource(scriptContext, scriptResourceName, typeof(UnitMeasure).Assembly);
            }
            catch (Exception exc)
            {
                throw new ApplicationException($"Не удалось выполнить скрипт события сущности\"{eventDisplayName}\":<br>" + exc.Message);
            }
		}
    }
}

