namespace Satel.Sppur.Frontend.Generated
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Castle.Windsor;
    using Castle.MicroKernel.Registration;
    using Microsoft.Extensions.DependencyInjection;
    using BarsUp.DataAccess;
	using BarsUp.Extensions.Configuration;
	using BarsUp.Extensions.Reflection;
    using BarsUp.Core.Application;
    
    using Satel.Sppur;
    using BarsUp.Application;
    using BarsUp.Windsor;
    using BarsUp;
    using BarsUp.Events;
    using BarsUp.IoC;
    using BarsUp.Registrar;
    
    using Satel.Sppur.Core;

    using BarsUp.Designer.Core;

    using BarsUp.Designer.GeneratedApp;
    using BarsUp.Utils;

    /// <summary>
    /// Класс подключения модуля
    /// </summary>
    [BarsUp.Utils.Display("СППУР [Frontend] (Генерируемый проект)")]    
    [BarsUp.Utils.Attributes.Uid("2af35a0a-e4db-cf8a-bd15-4b2007d9a4f7")]
    public class Module : AssemblyDefinedModule, IAspNetCoreServiceConfigurator
    {        
        /// <summary>
        /// Загрузка модуля
        /// </summary>
        public override void Install()
        {
            RegisterControllers();
RegisterFrontendServices();
RegisterServices();
RegisterViewServices();
AssemblyReplacedTypeResolver.AddAssemblyReplace("Satel.Sppur", GetType().Assembly.GetName().Name);
            
        }        
        
        protected override void SetDependencies()
        {
            base.SetDependencies();
            DependsOn<BarsUp.Designer.GeneratedApp.Module>();
DependsOn<Satel.Sppur.Core.Module>();
            
        }

        public void ConfigureServices(IServiceCollection services)
        {
            
            
        }

        private void RegisterControllers(){
    Container.RegisterEditorViewServiceController<Satel.Sppur.DataSource, Satel.Sppur.DataSourceEditorModel>("DataSourceEditor");
    Container.RegisterListViewServiceController<Satel.Sppur.DataSource, Satel.Sppur.DataSourceListModel>("DataSourceList");

}
private void RegisterFrontendServices(){
    Container.RegisterExtJsControllerClientRouteMapRegistrar<Satel.Sppur.ActionClientRoute>();

}
private void RegisterServices(){
    Container.RegisterResourceManifest<Satel.Sppur.Frontend.Generated.ResourceManifest>();

}
private void RegisterViewServices(){
    Container.RegisterEditorViewService<DataSourceEditorService>();
    Container.RegisterListViewService<DataSourceListService>();

}
    }
}
