namespace Satel.Sppur.Frontend.Generated
{
    using BarsUp;
    using BarsUp.UI.ExtJs;
	using BarsUp.UI.ExtJs.Compatibility4;
	using System.Collections.Generic;
	using System.Linq;
	using BarsUp.Utils;
	using BarsUp.Modules.Security;

    internal partial class ResourceManifest : ResourceManifestBase
    {
        protected override void AdditionalInit(IResourceManifestContainer container)
        {
		#region Перечисления

				

		#endregion

		#region Модели

		container.DefineExtJsModel<BarsUp.Modules.FileStorage.FileInfo>().ActionMethods(read: "POST").Controller("Empty");
container.DefineExtJsModel<DataSourceListModel>().ActionMethods(read: "POST").Controller("DataSourceList");
container.DefineExtJsModel<Satel.Sppur.DataSourceEditorModel>().ActionMethods(read: "POST").Controller("DataSourceEditor");

			

		#endregion
        }                
    }
}
