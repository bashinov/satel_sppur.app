namespace Satel.Sppur.Frontend.Generated
{
    using BarsUp;
    using BarsUp.Utils;

	/// <summary>
    /// Манифест ресурсов модуля
    /// </summary>
    internal partial class ResourceManifest
    {
		/// <summary>
        /// Базовая инициализация. 
        ///             Обычно вызывается из T4-шаблонов.
        /// </summary>
        /// <param name="container"/>
        protected override void BaseInit(IResourceManifestContainer container)
        {		
            
            RegisterResource(container, "content/css/form-styles-90f03dd9-22c1-4cec-b996-ab8325240153.css"); 
            RegisterResource(container, "libs/B4/autostart/SatelSppur.js"); 
            RegisterResource(container, "libs/B4/controller/DataSourceEditor.js"); 
            RegisterResource(container, "libs/B4/controller/DataSourceList.js"); 
            RegisterResource(container, "libs/B4/customVTypes/SatelSppur.js"); 
            RegisterResource(container, "libs/B4/view/DataSourceEditor.js"); 
            RegisterResource(container, "libs/B4/view/DataSourceList.js"); 
        }        
    }
}
