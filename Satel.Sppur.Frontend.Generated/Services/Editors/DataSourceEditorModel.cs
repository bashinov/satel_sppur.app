namespace Satel.Sppur
{
using Satel.Sppur.Core;
    using System;
    using System.Linq;
    using BarsUp;
    using BarsUp.DataAccess;
    using System.Collections.Generic;
        
    using BarsUp.Windsor;
    using BarsUp.IoC;
    using BarsUp.Utils;
    
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using BarsUp.Designer.GeneratedApp;
    using BarsUp.Designer.GeneratedApp.Editors;
    using BarsUp.Core.Exceptions;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Core.Serialization;

        /// <summary>
        /// Модель редактора 'Форма редактирования Источники данных'
        /// </summary>
        [BarsUp.Utils.DisplayAttribute("Форма редактирования Источники данных")]
        public class DataSourceEditorModel : DataTransferObject
        {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public DataSourceEditorModel(){
        }
        /// <summary>
        /// Свойство 'Наименование полное'
        /// </summary>
        [BarsUp.Utils.Display("Наименование полное")]
        [BarsUp.Utils.Attributes.Uid("89294a57-ef62-4103-84dc-d5f4126a5c4e")]
        public virtual System.String NameFull { get; set; }

        /// <summary>
        /// Свойство 'Наименование краткое'
        /// </summary>
        [BarsUp.Utils.Display("Наименование краткое")]
        [BarsUp.Utils.Attributes.Uid("0bbcf245-1dcc-4d8a-808a-f229fac52a04")]
        public virtual System.String NameCut { get; set; }

        /// <summary>
        /// Свойство 'Описание'
        /// </summary>
        [BarsUp.Utils.Display("Описание")]
        [BarsUp.Utils.Attributes.Uid("b3e91fcd-32af-4f34-9aaa-af8e7b9f268f")]
        public virtual System.String Comment { get; set; }

        }

}