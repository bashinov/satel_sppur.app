namespace Satel.Sppur
{
using Satel.Sppur.Core;
    using System;
    using System.Linq;
    using BarsUp;
    using BarsUp.DataAccess;
    using System.Collections.Generic;
    
    using System.Diagnostics;
    using BarsUp.Windsor;
    using BarsUp.IoC;
    using BarsUp.Utils;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.Designer.GeneratedApp.Editors;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Core.Exceptions;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Modules.Filter;
    using BarsUp.Designer.Filter;
    using BarsUp.Extensions.Json;
    using BarsUp.Modules.Fias;
    using BarsUp.Modules.FileStorage;
    using BarsUp.Utils.Annotations;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Castle.Windsor;
    using Castle.MicroKernel.Registration;
    using BarsUp.Modules.PostgreSql.DataAccess;
            
    using System.Threading;
    using System.Threading.Tasks;
    using NHibernate;
    using NHibernate.Linq;
    using BarsUp.Modules.EntityAttributes.Extensions;

    /// <summary>
	/// Контракт сервиса редактора 'Форма редактирования Источники данных'
	/// </summary>
	public interface IDataSourceEditorService : IEditorViewService<Satel.Sppur.DataSource,DataSourceEditorModel>
	{
	
	}
    
    /// <summary>
	/// Интерфейс обработчика редактора 'Форма редактирования Источники данных'
	/// </summary>
    public interface IDataSourceEditorServiceHandler : IEditorViewServiceHandler<Satel.Sppur.DataSource,DataSourceEditorModel>
    {
    }

    /// <summary>
	/// Базовый класс обработчика редактора 'Форма редактирования Источники данных'
	/// </summary>
    public abstract class AbstractDataSourceEditorServiceHandler : EditorViewServiceHandler<Satel.Sppur.DataSource,DataSourceEditorModel>, IDataSourceEditorServiceHandler
    {        
    }
    
    /// <summary>
	/// Базовый класс обработчика редактора 'Форма редактирования Источники данных', который может сравнивать состояния моделей
	/// </summary>
    public abstract class AbstractDataSourceEditorServiceModelComparerHandler : EditorViewServiceModelComparerHandler<Satel.Sppur.DataSource,DataSourceEditorModel>, IDataSourceEditorServiceHandler
    {        
    }
    
    /// <summary>
    /// Реализация модели представления 'Форма редактирования Источники данных'
    /// </summary>
    public class DataSourceEditorService : EditorViewService<Satel.Sppur.DataSource, DataSourceEditorModel>,  IDataSourceEditorService
    {                

        public DataSourceEditorService(IWindsorContainer container)
            :base(container)
        {
        }

        
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override async Task<DataSourceEditorModel> CreateModelInternalAsync(BaseParams @params, CancellationToken cancellationToken)
        {
            await Task.CompletedTask;
            var model = new DataSourceEditorModel();			
			            
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Источники данных' в модель представления
        /// </summary>
        protected override async Task<DataSourceEditorModel> MapEntityInternalAsync(Satel.Sppur.DataSource entity, BaseParams baseParams, CancellationToken cancellationToken)
        {
            await Task.CompletedTask;
            // создаем экзепляр модели
            var model = new DataSourceEditorModel();
            model.Id = entity.Id;
            model.NameFull = ((System.String)(entity.NameFull));
            model.NameCut = ((System.String)(entity.NameCut));
            model.Comment = ((System.String)(entity.Comment));
            			
            
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Источники данных' из модели представления		
        /// </summary>
        protected override async Task UnmapEntityInternalAsync(Satel.Sppur.DataSource entity, DataSourceEditorModel model, IDictionary<string, FileData> requestFiles, IList<BarsUp.Modules.FileStorage.FileInfo> filesToDelete, CancellationToken cancellationToken, BaseParams baseParams, bool isNested = false)
        {            
            await Task.CompletedTask;
            entity.NameFull = model.NameFull;
            entity.NameCut = model.NameCut;
            entity.Comment = model.Comment;
           
        }
        
        protected override async Task SaveInnerEditorsInternalAsync(Satel.Sppur.DataSource entity, DataSourceEditorModel model,
            IDictionary<string, FileData> requestFiles, IList<BarsUp.Modules.FileStorage.FileInfo> filesToDelete,
            bool isNested, BaseParams baseParams, CancellationToken cancellationToken)
        {
            await Task.CompletedTask;
                    }

    }
}