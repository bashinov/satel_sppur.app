namespace Satel.Sppur
{
using Satel.Sppur.Core;
    using System;
    using System.Linq;
    using BarsUp;
    using BarsUp.DataAccess;
    using System.Collections.Generic;
    
    using System.Diagnostics;
    using BarsUp.Windsor;
    using BarsUp.IoC;
    using BarsUp.Utils;
    using Newtonsoft.Json;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Core.Serialization;

    /// <summary>
    /// Модель записи реестра 'Реестр Источники данных'
    /// </summary>
	[BarsUp.Utils.DisplayAttribute("Реестр Источники данных")]
    public class DataSourceListModel  : DataTransferObject
    {
        /// <summary>
        /// Поле 'Наименование полное' (псевдоним: NameFull)
        /// </summary>
        [BarsUp.Utils.Display("Наименование полное")]
        [BarsUp.Utils.Attributes.Uid("5c3fee23-c4c7-4856-bf16-963845a4ba51")]
        public virtual System.String NameFull { get; set; }

        /// <summary>
        /// Поле 'Наименование краткое' (псевдоним: NameCut)
        /// </summary>
        [BarsUp.Utils.Display("Наименование краткое")]
        [BarsUp.Utils.Attributes.Uid("7895b244-9363-4727-8505-a3103fe3ff4e")]
        public virtual System.String NameCut { get; set; }

        /// <summary>
        /// Поле 'Описание' (псевдоним: Comment)
        /// </summary>
        [BarsUp.Utils.Display("Описание")]
        [BarsUp.Utils.Attributes.Uid("a5504987-f2c9-4c7c-b485-0058caa5aba9")]
        public virtual System.String Comment { get; set; }

    }	
}