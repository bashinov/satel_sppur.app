   
namespace Satel.Sppur
{
using Satel.Sppur.Core;
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using BarsUp;
    using BarsUp.DataAccess; 
    using System.Collections.Generic;
    
    using System.Diagnostics;
    using BarsUp.Windsor;
    using BarsUp.IoC;
    using BarsUp.Utils;
    
    using Castle.Windsor;
	using NHibernate.Linq;

    using BarsUp.Designer.GeneratedApp;
    using BarsUp.Designer.GeneratedApp.Attributes;
    using BarsUp.Designer.GeneratedApp.Queries;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using BarsUp.Extensions.Json;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Lists;
	using BarsUp.DataAccess.Extentions;    
    
    using System.Threading;
    using System.Threading.Tasks;

    using BarsUp.DataAccess.HqlGenerators;

	/// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Источники данных'
    /// </summary>
	public interface IDataSourceListService : IListViewService<Satel.Sppur.DataSource, Satel.Sppur.DataSourceListModel, BaseParams>
	{
        	}

	/// <summary>
    /// Интерфейс фильтра для сервиса представления 'Реестр Источники данных'
    /// </summary>
	public interface IDataSourceListServiceFilter : IListViewServiceFilter<Satel.Sppur.DataSource, Satel.Sppur.DataSourceListModel, BaseParams>
	{		
	}

    /// <summary>
    /// Интерфейс обработчика для сервиса представления 'Реестр Источники данных'
    /// </summary>
    public interface IDataSourceListServiceHandler : IListViewServiceHandler<Satel.Sppur.DataSource, Satel.Sppur.DataSourceListModel, BaseParams>
    {

    }

    /// <summary>
    /// Базовый класс обработчика списка 'Реестр Источники данных'
    /// </summary>
    public abstract class AbstractDataSourceListServiceHandler : ListViewServiceHandler<Satel.Sppur.DataSource, Satel.Sppur.DataSourceListModel>, IDataSourceListServiceHandler
    {        
    }

    /// <summary>
    /// Сервис для представления 'Реестр Источники данных'
    /// </summary>
	[BarsUp.Utils.DisplayAttribute("Реестр Источники данных")]
	    public class DataSourceListService : ListViewService<Satel.Sppur.DataSource, Satel.Sppur.DataSourceListModel>, IDataSourceListService 
    {        
		/// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
		{
			get { return @"[]
"; }
		}
                                  
        public DataSourceListService(IWindsorContainer container) 
            : base(container) { }

		/// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name="entityQuery">Запрос сущностей</param>
        /// <param name="params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Satel.Sppur.DataSource> Filter(IQueryable<Satel.Sppur.DataSource> entityQuery, BaseParams @params)
		{
            var query = base.Filter(entityQuery, @params);
            
            if (@params.IsNotNull())
            {                
				
                
                            }
            
            return query;
		}
  
        
        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Satel.Sppur.DataSourceListModel> Map(IQueryable<Satel.Sppur.DataSource> entityQuery)
        {			
            // формирование селектора			
            return entityQuery.IsMaterialized()
                ? entityQuery.AsEnumerable().Select(x => new Satel.Sppur.DataSourceListModel {
                 Id = x?.Id,
                    _TypeUid = "a5cdbc25-bb2c-42d7-a1c6-e81c9171e117",
                    NameFull = ((System.String)(x?.NameFull)),
                    NameCut = ((System.String)(x?.NameCut)),
                    Comment = ((System.String)(x?.Comment)),
                }).AsQueryable()
                : entityQuery.Select(x => new Satel.Sppur.DataSourceListModel {
                Id = x.Id,
                    _TypeUid = "a5cdbc25-bb2c-42d7-a1c6-e81c9171e117",
                    NameFull = ((System.String)(x.NameFull)),
                    NameCut = ((System.String)(x.NameCut)),
                    Comment = ((System.String)(x.Comment)),
            });
        }

        
        
        protected override async Task ProcessResultAsync(IList<Satel.Sppur.DataSourceListModel> elements, BaseParams @params, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            // устанавливаем контроллер редактора для каждой из сущностей
foreach (var itm in elements)
{
    switch (itm._TypeUid)
    {
        case "a5cdbc25-bb2c-42d7-a1c6-e81c9171e117":
            itm._EditorName = "DataSourceEditor";
            break;
    }
}

            await Task.CompletedTask;
        }

            }
}