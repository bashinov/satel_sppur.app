namespace Satel.Sppur
{
using Satel.Sppur.Core;
    /// <summary>
    /// Маппинг UID реестров/форм
    /// </summary>
    public class SppurRmsViewStackMap
    {
        /// <summary>
/// Элемент 'Форма редактирования Источники данных'
/// </summary>
public const string DataSourceEditorForm = "90f03dd9-22c1-4cec-b996-ab8325240153";

/// <summary>
/// Элемент 'Реестр Источники данных'
/// </summary>
public const string DataSourceListListView = "d91f85ce-0b6a-49e5-a46f-74782ba54242";

	}
}