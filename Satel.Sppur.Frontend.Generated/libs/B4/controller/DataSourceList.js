Ext.define('B4.controller.DataSourceList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.DataSourceList',
        'B4.model.DataSourceListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-datasourcelist',
    viewUid: 'd91f85ce-0b6a-49e5-a46f-74782ba54242',
    viewDataName: 'DataSourceList',
    // набор описаний действий реестра
    actions: {
        'Addition-DataSourceEditor-InWindow': {
            'editor': 'DataSourceEditor',
            'editorAlias': 'rms-datasourceeditor',
            'editorUid': '90f03dd9-22c1-4cec-b996-ab8325240153',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-DataSourceEditor-InWindow': {
            'editor': 'DataSourceEditor',
            'editorAlias': 'rms-datasourceeditor',
            'editorUid': '90f03dd9-22c1-4cec-b996-ab8325240153',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-datasourcelist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    // Не открываем редактирование, если спрятан верхний тулбар
                    if (sender.panel.dockedItems && !sender.panel.dockedItems.items.filter(x => x.dock == 'top' && x.xtype == 'toolbar').length) return;
                    this.defaultEditActionHandler(false, 'Editing-DataSourceEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-datasourcelist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
    },
});