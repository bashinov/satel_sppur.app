Ext.define('B4.view.DataSourceEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-datasourceeditor',
    title: 'Форма редактирования Источники данных',
    rmsUid: '90f03dd9-22c1-4cec-b996-ab8325240153',
    rmsElementUid: '90f03dd9-22c1-4cec-b996-ab8325240153',
    dataSourceUid: 'a5cdbc25-bb2c-42d7-a1c6-e81c9171e117',
    requires: [
        'B4.form.field.CodeEditor'],
    statics: {
        windowSize: {}
    },
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'DataSource_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'NameFull',
        'modelProperty': 'DataSource_NameFull',
        'type': 'TextField'
    }, {
        'dataIndex': 'NameCut',
        'modelProperty': 'DataSource_NameCut',
        'type': 'TextField'
    }, {
        'dataIndex': 'Comment',
        'modelProperty': 'DataSource_Comment',
        'type': 'CodeEditorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '90f03dd9-22c1-4cec-b996-ab8325240153-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'DataSourceEditor-container',
        'layout': {
            'type': 'anchor'
        },
        'region': 'Center',
        items: [{
            'anchor': '100%',
            'cls': 'cmp-style-90f03dd9-22c1-4cec-b996-ab8325240153-2aa36047-ca42-451a-a1c9-98eb6e5e1bfa',
            'dockedItems': [],
            'items': [{
                'cls': 'cmp-style-90f03dd9-22c1-4cec-b996-ab8325240153-be43056d-e66a-4cf4-9cbc-3b769b233013',
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'anchor': '100%',
                    'cls': 'cmp-style-90f03dd9-22c1-4cec-b996-ab8325240153-89294a57-ef62-4103-84dc-d5f4126a5c4e',
                    'dataIndexAbsoluteUid': 'FieldPath://a5cdbc25-bb2c-42d7-a1c6-e81c9171e117$e052d138-d463-4df9-8053-3cd41fd05c7d',
                    'fieldLabel': 'Наименование полное',
                    'labelAlign': 'top',
                    'modelProperty': 'DataSource_NameFull',
                    'name': 'NameFull',
                    'rmsUid': '89294a57-ef62-4103-84dc-d5f4126a5c4e',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'be43056d-e66a-4cf4-9cbc-3b769b233013',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '2aa36047-ca42-451a-a1c9-98eb6e5e1bfa',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'cls': 'cmp-style-90f03dd9-22c1-4cec-b996-ab8325240153-275d82cd-16be-40e8-b3e1-917989a698f9',
            'dockedItems': [],
            'items': [{
                'cls': 'cmp-style-90f03dd9-22c1-4cec-b996-ab8325240153-8e564fe2-6ec8-4380-b767-d459adcd00c7',
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'anchor': '100%',
                    'cls': 'cmp-style-90f03dd9-22c1-4cec-b996-ab8325240153-0bbcf245-1dcc-4d8a-808a-f229fac52a04',
                    'dataIndexAbsoluteUid': 'FieldPath://a5cdbc25-bb2c-42d7-a1c6-e81c9171e117$0cd3d854-08e9-427b-bc9c-194139dbca16',
                    'fieldLabel': 'Наименование краткое',
                    'labelAlign': 'top',
                    'modelProperty': 'DataSource_NameCut',
                    'name': 'NameCut',
                    'rmsUid': '0bbcf245-1dcc-4d8a-808a-f229fac52a04',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': '8e564fe2-6ec8-4380-b767-d459adcd00c7',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '275d82cd-16be-40e8-b3e1-917989a698f9',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'cls': 'cmp-style-90f03dd9-22c1-4cec-b996-ab8325240153-f0226651-5a52-4fb6-b4b3-1263db49cf0b',
            'dockedItems': [],
            'items': [{
                'cls': 'cmp-style-90f03dd9-22c1-4cec-b996-ab8325240153-e100e2c1-1647-482f-9148-ca2e88a9cb62',
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'anchor': '100%',
                    'cls': 'cmp-style-90f03dd9-22c1-4cec-b996-ab8325240153-b3e91fcd-32af-4f34-9aaa-af8e7b9f268f',
                    'dataIndexAbsoluteUid': 'FieldPath://a5cdbc25-bb2c-42d7-a1c6-e81c9171e117$573db6de-9ac5-4b27-8fd4-bad7a199c036',
                    'fieldLabel': 'Описание',
                    'labelAlign': 'top',
                    'modelProperty': 'DataSource_Comment',
                    'name': 'Comment',
                    'rmsUid': 'b3e91fcd-32af-4f34-9aaa-af8e7b9f268f',
                    'xtype': 'b4codeEditor'
                }],
                'layout': 'anchor',
                'rmsUid': 'e100e2c1-1647-482f-9148-ca2e88a9cb62',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'f0226651-5a52-4fb6-b4b3-1263db49cf0b',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'DataSource_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    plugins: [],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    beforeLoadRecord: function(entityId) {
        var me = this;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('NameFull'));
        }
        return res;
    },
});