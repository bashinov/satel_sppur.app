Ext.define('B4.view.DataSourceList', {
    'alias': 'widget.rms-datasourcelist',
    'dataSourceUid': 'a5cdbc25-bb2c-42d7-a1c6-e81c9171e117',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.DataSourceListModel',
    'rmsElementUid': 'd91f85ce-0b6a-49e5-a46f-74782ba54242',
    'rmsUid': 'd91f85ce-0b6a-49e5-a46f-74782ba54242',
    'stateful': false,
    'title': 'Реестр Источники данных',
    requires: [
        'B4.model.DataSourceListModel',
        'B4.ux.grid.plugin.StyleConditions',
        'BarsUp.filter.ExtGridPlugin'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'cls': 'cmp-style-d91f85ce-0b6a-49e5-a46f-74782ba54242-417d19d5-f475-4708-ac57-959b099a5bab',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'ignoreMakeInactive': false,
        'itemId': 'Refresh',
        'rmsUid': '417d19d5-f475-4708-ac57-959b099a5bab',
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'cls': 'cmp-style-d91f85ce-0b6a-49e5-a46f-74782ba54242-379d0231-2ed7-4e83-a036-47d5b8f03921',
        'hidden': false,
        'iconCls': 'icon-add',
        'ignoreMakeInactive': false,
        'itemId': 'Addition-DataSourceEditor-InWindow',
        'rmsUid': '379d0231-2ed7-4e83-a036-47d5b8f03921',
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'cls': 'cmp-style-d91f85ce-0b6a-49e5-a46f-74782ba54242-897d5514-dbf0-4eef-934c-1fcd386510e6',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'ignoreMakeInactive': false,
        'itemId': 'Editing-DataSourceEditor-InWindow',
        'rmsUid': '897d5514-dbf0-4eef-934c-1fcd386510e6',
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'cls': 'cmp-style-d91f85ce-0b6a-49e5-a46f-74782ba54242-ff716de8-bd0e-4917-8e08-17c74d785c40',
        'hidden': false,
        'iconCls': 'icon-delete',
        'ignoreMakeInactive': false,
        'itemId': 'Deletion',
        'rmsUid': 'ff716de8-bd0e-4917-8e08-17c74d785c40',
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'NameFull',
                'dataIndexAbsoluteUid': 'FieldPath://a5cdbc25-bb2c-42d7-a1c6-e81c9171e117$e052d138-d463-4df9-8053-3cd41fd05c7d',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'rmsUid': '5c3fee23-c4c7-4856-bf16-963845a4ba51',
                'sortable': true,
                'text': 'Наименование полное',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'NameCut',
                'dataIndexAbsoluteUid': 'FieldPath://a5cdbc25-bb2c-42d7-a1c6-e81c9171e117$0cd3d854-08e9-427b-bc9c-194139dbca16',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'rmsUid': '7895b244-9363-4727-8505-a3103fe3ff4e',
                'sortable': true,
                'text': 'Наименование краткое',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Comment',
                'dataIndexAbsoluteUid': 'FieldPath://a5cdbc25-bb2c-42d7-a1c6-e81c9171e117$573db6de-9ac5-4b27-8fd4-bad7a199c036',
                'decimalPrecision': 2,
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'hidden': true,
                'menuDisabled': false,
                'rmsUid': 'a5504987-f2c9-4c7c-b485-0058caa5aba9',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Описание',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'gridfilters',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }]
        });
        me.callParent(arguments);
    }
});