// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
namespace Satel.Sppur.Frontend.Tests.Clickers
{
using Satel.Sppur.Frontend;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Castle.Windsor;    

    using BarsUp.DataAccess;    
    using BarsUp.Utils;    
    using BarsUp.QualityAssurance.Web;
    using BarsUp.QualityAssurance.Web.ExtJs;        
    using BarsUp.QualityAssurance.Web.PageObjects;        

    using Satel.Sppur.Frontend;
    using Satel.Sppur.Frontend.Tests;    
    using Satel.Sppur.Frontend.Tests.PageObjects;

    using Xunit;
    using Xunit.Abstractions;
    using OpenQA.Selenium;

    /// <summary>    
    /// Класс-кликер, содержащий код для обхода редактора Форма редактирования Источники данных
    /// </summary>       
    [Display("Кликер редактора [Форма редактирования Источники данных]")]
    public class DataSourceEditorClicker : WebElementClicker<DataSourceEditorPageObject>
    {          
        public DataSourceEditorClicker(AbstractWebDriverTest test, DataSourceEditorPageObject view) : base(test, view) {}

        public override void Run()
        {
                    }                
    }
}