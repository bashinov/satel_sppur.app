// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
namespace Satel.Sppur.Frontend.Tests.PageObjects
{
using Satel.Sppur.Frontend;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Castle.Windsor;    

    using BarsUp.DataAccess;    
    using BarsUp.Utils;    
    using BarsUp.QualityAssurance.Web;
    using BarsUp.QualityAssurance.Web.ExtJs;        
    using BarsUp.QualityAssurance.Web.PageObjects;        

    using Satel.Sppur.Frontend;
    using Satel.Sppur.Frontend.Tests;        

    using Xunit;
    using Xunit.Abstractions;
    using OpenQA.Selenium;    

    /// <summary>    
    /// Описание формы 'Форма редактирования Источники данных' для ui-тестирования
    /// </summary>       
    [ExtJsXType("rms-datasourceeditor")]
    [Display("Редактор [Форма редактирования Источники данных]")]
    public class DataSourceEditorPageObject : BarsUpGeneratedEditorView<DataSourceEditorModel>
    {  
        public DataSourceEditorPageObject(ExtJsComponent initializer, string elementId) : base(initializer, elementId) {}

        public DataSourceEditorPageObject(WebDriverAbstractFixture context, string elementId, ExtJsComponent initializer = null) : base(context, elementId, initializer) {}

        
/// <summary>
/// Однострочный редактор текста [Наименование полное]
/// Идентификатор [89294a57-ef62-4103-84dc-d5f4126a5c4e]
/// Поле сущности: Источники данных.Наименование полное
/// </summary>
[Display("Наименование полное")]
public ExtJsFormField<System.String> Наименование_полное { get; internal set; }

/// <summary>
/// Однострочный редактор текста [Наименование краткое]
/// Идентификатор [0bbcf245-1dcc-4d8a-808a-f229fac52a04]
/// Поле сущности: Источники данных.Наименование краткое
/// </summary>
[Display("Наименование краткое")]
public ExtJsFormField<System.String> Наименование_краткое { get; internal set; }

/// <summary>
/// Редактор кода [Описание]
/// Идентификатор [b3e91fcd-32af-4f34-9aaa-af8e7b9f268f]
/// Поле сущности: Источники данных.Описание
/// </summary>
[Display("Описание")]
public ExtJsFormField<System.String> Описание { get; internal set; }
        
        protected override void Initialize()
        {
            base.Initialize();
            Наименование_полное = Down<ExtJsFormField<System.String>>("[rmsUid=89294a57-ef62-4103-84dc-d5f4126a5c4e]");
Наименование_краткое = Down<ExtJsFormField<System.String>>("[rmsUid=0bbcf245-1dcc-4d8a-808a-f229fac52a04]");
Описание = Down<ExtJsFormField<System.String>>("[rmsUid=b3e91fcd-32af-4f34-9aaa-af8e7b9f268f]");
        }

            }
}