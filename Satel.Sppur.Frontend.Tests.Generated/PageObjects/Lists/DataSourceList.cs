// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
namespace Satel.Sppur.Frontend.Tests.PageObjects
{
using Satel.Sppur.Frontend;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Castle.Windsor;    

    using BarsUp.DataAccess;    
    using BarsUp.Utils;    
    using BarsUp.QualityAssurance.Web;
    using BarsUp.QualityAssurance.Web.ExtJs;        
    using BarsUp.QualityAssurance.Web.PageObjects;        

    using Satel.Sppur.Frontend;
    using Satel.Sppur.Frontend.Tests;        

    using Xunit;
    using Xunit.Abstractions;
    using OpenQA.Selenium;

    /// <summary>    
    /// Описание реестра 'Реестр Источники данных' для ui-тестирования
    /// </summary>       
    [ExtJsXType("rms-datasourcelist")]
    [Display("Список [Реестр Источники данных]")]
    public class DataSourceListPageObject : BarsUpGeneratedListView<DataSourceListModel>
    {  
        public DataSourceListPageObject(ExtJsComponent initializer, string elementId) : base(initializer, elementId) {}

        public DataSourceListPageObject(WebDriverAbstractFixture context, string elementId, ExtJsComponent initializer = null) : base(context, elementId, initializer) {}

        [Display("Верхняя_панель")]
public Toolbar_Верхняя_панель Верхняя_панель { get; internal set; }
        
        protected override void Initialize()
        {
            base.Initialize();
            Верхняя_панель = Down<Toolbar_Верхняя_панель>("[dock=top]");
        }

        /// <summary>
/// Панель действие реестра 'Верхняя панель'
/// </summary>
public class Toolbar_Верхняя_панель : ExtJsToolbar {
    /// <summary>
    /// Действие обновления данных реестра
    /// </summary>
    [Display("Обновить")]
    public ExtJsButton Обновить { get; private set; }
    /// <summary>
    /// Действие добавления записи реестра
    /// </summary>
    [Display("Добавить")]
    public ExtJsButton Добавить { get; private set; }
    /// <summary>
    /// Действие редактирования записи реестра
    /// Редактор: 90f03dd9-22c1-4cec-b996-ab8325240153
    /// Режим: В окне
    /// </summary>
    [Display("Редактировать")]
    public ExtJsListViewEditAction<DataSourceEditorPageObject> Редактировать { get; private set; }

    public Toolbar_Верхняя_панель(ExtJsComponent initializer, string elementId) : base(initializer, elementId) {}
    public Toolbar_Верхняя_панель(WebDriverAbstractFixture context, string elementId, ExtJsComponent initializer = null) : base(context, elementId, initializer) {}
    protected override void Initialize() {
        base.Initialize();
        Обновить = Down<ExtJsButton>("[rmsUid=417d19d5-f475-4708-ac57-959b099a5bab]");
        Добавить = Down<ExtJsButton>("[rmsUid=379d0231-2ed7-4e83-a036-47d5b8f03921]");
        Редактировать = Down<ExtJsListViewEditAction<DataSourceEditorPageObject>>("[rmsUid=897d5514-dbf0-4eef-934c-1fcd386510e6]", 
            x => {
                x.EditorUid = "90f03dd9-22c1-4cec-b996-ab8325240153";
                x.Mode = ExtJsListViewEditActionMode.InWindow;
                x.ListView = (ExtJsGridPanel)Initializer;
            }
        );

    }
}

    }
}