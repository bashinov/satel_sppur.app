namespace Satel.Sppur.Frontend
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Castle.Windsor;
    using Castle.MicroKernel.Registration;
    using Microsoft.Extensions.DependencyInjection;
    using BarsUp.DataAccess;
	using BarsUp.Extensions.Configuration;
	using BarsUp.Extensions.Reflection;
    using BarsUp.Core.Application;
    
    using Satel.Sppur;
    using BarsUp.Application;
    using BarsUp.Windsor;
    using BarsUp;
    using BarsUp.Events;
    using BarsUp.IoC;
    using BarsUp.Registrar;
    
    using Satel.Sppur.Frontend.Generated;
    using BarsUp.Utils;

    /// <summary>
    /// Класс подключения модуля
    /// </summary>
    [BarsUp.Utils.Display("СППУР [Frontend]")]    
    [BarsUp.Utils.Attributes.Uid("830f5eaa-8850-462f-84c6-79138d215f90")]
    public class Module : AssemblyDefinedModule, IAspNetCoreServiceConfigurator
    {        
        /// <summary>
        /// Загрузка модуля
        /// </summary>
        public override void Install()
        {
            AssemblyReplacedTypeResolver.AddAssemblyReplace("Satel.Sppur", GetType().Assembly.GetName().Name);
            
        }        
        
        protected override void SetDependencies()
        {
            base.SetDependencies();
            DependsOn<BarsUp.Designer.GeneratedApp.Module>();
DependsOn<Satel.Sppur.Frontend.Generated.Module>();
            
        }

        public void ConfigureServices(IServiceCollection services)
        {
            
            
        }

            }
}
