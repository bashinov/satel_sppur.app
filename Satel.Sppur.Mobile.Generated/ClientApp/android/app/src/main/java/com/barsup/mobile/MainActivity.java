package com.barsup.satel_sppur;

import android.os.Bundle;
import com.getcapacitor.BridgeActivity;
import com.getcapacitor.Plugin;
import java.util.ArrayList;
import com.getcapacitor.community.fcm.FCMPlugin;
import com.bkon.capacitor.fileselector.FileSelector;

public class MainActivity extends BridgeActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initializes the Bridge
        this.init(
                savedInstanceState,
                new ArrayList<Class<? extends Plugin>>() {
                    {
                        // Additional plugins you've installed go here
                        // Ex: add(TotallyAwesomePlugin.class);
                        add(FCMPlugin.class);
                        add(FileSelector.class);
                    }
                }
            );
    }
}
